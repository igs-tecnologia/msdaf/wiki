---
title: Sobre o Projeto
date: 2019-03-11 12:09:42
toc: true

sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

categories:
- [Sobre, Equipe]
- [Sobre, Projeto]
- [Sobre, Atuação]
- [Sobre, GAESI]
---



## A assistência Farmacêutica

A **Assistência Farmacêutica** é o conjunto de ações voltadas à promoção, proteção e recuperação da saúde, por meio da promoção do acesso e uso racional de medicamentos que são desenvolvidas pelo Ministério da Saúde.

A oferta de medicamentos no Sistema Único de Saúde (SUS) é organizada em três componentes que compõem o Bloco de Financiamento da Assistência Farmacêutica - Básico, Estratégico e Especializado, além do Programa Farmácia Popular. Com exceção do Farmácia Popular, em todos os outros componentes o financiamento e a escolha de qual componente o medicamento fará parte é tripartite, ou seja, a responsabilidade é da União, dos estados e os municípios.

#### Componente Básico

Os medicamentos que estão disponíveis no Componente Básico da Assistência Farmacêutica são destinados à Atenção Básica à Saúde. A responsabilidade pela aquisição, seleção, armazenamento, controle de estoque e prazos de validade, além da distribuição e dispensação destes medicamentos é dos estados, do Distrito Federal e dos municípios. Os recursos federais são repassados mensalmente, do Fundo Nacional de Saúde para os fundos estaduais e/ou municipais de saúde com base na população IBGE e encontram-se regulares em todo o país.

#### Componente Estratégico

Os recursos financeiros do Ministério da Saúde para o componente Estratégico são para aquisição de medicamentos de programas considerados estratégicos no cuidado da saúde, também dispensados na rede básica de saúde. São medicamentos para o tratamento de Tuberculose, Hanseníase, o Combate ao Tabagismo, para a Alimentação e Nutrição e para as Endemias Focais (Ex: Malária, Leishmaniose, Dengue, dentre outras), Coagulopatias e DST/AIDS. Esses medicamentos são adquiridos pelo Ministério da Saúde e distribuídos aos estados e/ou municípios de acordo com programação informada pelos Estados e Municípios.

#### Componente Especializado

O componente Especializado possui como principal característica a garantia da integralidade do tratamento medicamentoso para todas as doenças contempladas em Protocolos Clínicos e Diretrizes Terapêuticas (PCDT) por meio das diferentes linhas de cuidado.

É constituído por medicamentos que representam elevado impacto financeiro, por aqueles indicados para doenças mais complexas e para os casos de refratariedade ou intolerância a primeira e/ou a segunda linha de tratamento. Alguns medicamentos estão sob competência do Ministério da Saúde, outros dos Estados e demais dos municípios.

#### Farmácia Popular

Como forma complementar aos Componentes de Medicamentos por meio das farmácias credenciadas no Programa “Aqui tem Farmácia Popular”, a população pode adquirir 14 medicamentos para hipertensão, diabetes e asma sem custo. Além disso, são ofertados descontos de até 90% em 10 medicamentos para rinite, dislipidemia, Parkinson, osteoporose, glaucoma, contraceptivos, além das fraldas geriátricas.

Esse benefício não está direcionado somente aos usuários do Sistema Único de Saúde (SUS). Isso significa que mesmo estando com uma receita de uma clínica ou médico particular em mãos, quem precisar adquirir esses medicamentos e correlatos poderá contar com os descontos do Programa.

## Categorias relacionadas

A categoria base para conteúdos relacionados a esta área é [Sobre](/msdaf/wiki/categories/Sobre). Alguns conteúdos adicionais relacionados com o projeto em si foram adicionados por caloboradores por meio das seguintes categorias:

#### [Equipe](/msdaf/wiki/categories/Equipe/)

Conteúdos relacionados à equipe envolvida, não somente no desenvolvimento tecnológico, mas também na elicitação de requisitos, identificação e manipulação de dados e demais atividades secundárias ao objeto deste trabalho.

#### [Projeto](/msdaf/wiki/categories/Projeto/)

Conteúdos relcionados aos acontecimentos do projeto em si, processos governamentais e jurídicos que tenham impacto no cronograma, notificações e informativos gerais.

#### [Atuação](/msdaf/wiki/categories/Atuação/)

Conteúdos sobre as atuações e resultados obtidos por meio dos trabalhos desenvolvidos, publicações científicas e demais artefatos produzidos.

#### [GAESI](/msdaf/wiki/categories/GAESI/)

Conteúdos que de alguma forma se relacionam com o GAESI.
