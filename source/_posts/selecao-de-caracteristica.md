---
title: Meios e Metodos de Seleção de Caracteristicas de Dados
date: 2019-07-23 12:40:04
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
- [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]

tags:
---

## **Análise Fatorial**

Segundo Y. Lakshmi Prasad a Análise Fatorial é utilizada nas seguintes ocasiões:

> Quando temos um grande número de variáveis ​​em nosso conjunto de dados e precisamos reduzir esse número  
>   
> Antes de executar a regressão ou a análise de cluster em um conjunto de dados com variáveis ​​correlacionadas.  
>   
> Ao analisar os resultados da pesquisa, onde as respostas a muitas perguntas tendem a ser altamente correlacionadas.[1]  


## **Análise de Componentes Principais (PCA)**

> PCA é uma transformação ortogonal linear que transforma os dados em um novo sistema de coordenadas de modo que a maior variação por qualquer projeção dos dados chegue à primeira coordenada, a segunda maior variação na segunda coordenada, e assim por diante. [1]  


> A ideia central da análise de componentes principais (PCA) é reduzir a dimensionalidade de um conjunto de dados que consiste em um grande número de variáveis inter-relacionadas, mantendo o máximo possível a variação presente no conjunto de dados. Isto é conseguido através da transformação de um novo conjunto de variáveis, os componentes principais (PCs), que não são correlacionados, e que são ordenados de modo que os primeiros retenham a maior parte da variação presente em todas as variáveis originais [2]		  


## **Decomposição de Valor Singular (SVD)**

> A decomposição de autovalores é um método muito bom para extrair as características da matriz, mas ela é efetiva apenas para matriz quadrada, no entanto, a maior parte da matriz não é quadrada no mundo real. Por exemplo, existem N alunos, cada aluno tem pontuações M, uma matriz NxM não seria quadrada. Como podemos descrever as características importantes dessa matriz comum? A decomposição de valor singular (logo como VSD) pode ser usada para fazer isso, e a decomposição de valor singular é um método que pode ser aplicado a qualquer matriz. [2]  


Com o analisado podemos perceber que o PCA é o método mais robusto e com isso o mais utilizado levanto em consideração que a maioria massa de dados trabalhadas possuem múltiplas dimensões, quando isso não ocorre o melhor método a se aplicar é o SVD que trabalha apenas com dados Bi-dimensionais. Já a Análise Fatorial tem *são usados ​​quando o objetivo é descrever melhor os dados e o componente, para serem utilizados para reduzir a quantidade de dados. [1]*

## Dropout
A técnica de Dropout consiste em randomicamente zerar os pesos e/ou os valores de algumas dimensões e/ou registros a fim de gerar mais um parametros de teste da real impacto das dimensões on resultado aferido.

## Matriz de Correlação com Heatmap
Matrizes de confusão, bem como a de correlação são utilizadas para a seleção de características de forma analítica, enquanto métodos como o dropout podem ser feitos computacionalmente através de repetição e verificação da queda do erro e aumento da acurácia.
 

## Referências
[1] 	Prasad, Y. L. (2016). Big Data Analytics Made Easy. In *Notion Press*.

[2]   Wang, Y., & Zhu, L. (2017). Research and implementation of SVD in machine learning. *Proceedings - 16th IEEE/ACIS International Conference on Computer and Information Science, ICIS 2017*, 471–475.
