---
title: RIPSA (Rede interagencial de informações para a Saúde) & Meteor (Metadados online).
date: 2019-08-06 10:06:04
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
- [Preditivo, Levantamento Epidemiológico]
- [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]

tags:
---
### ***RIPSA***

[1]Tem como propósito promover a disponibilidade adequada e oportuna de dados básicos, indicadores e análises sobre as condições de saúde e suas tendências, visando aperfeiçoar a capacidade de formulação, gestão e avaliação de políticas e ações públicas pertinentes.


### Objetivos da RIPSA


>[1] **Objetivo:**
> - Estabelecer base de informações essenciais e consistentes para a análise das condições de saúde no País, facilmente acessíveis pelos diversos tipos de usuários e construídas mediante processo interinstitucional de trabalho;
> -  Articular a participação de instituições que contribuam para a produção, crítica e análise de dados e indicadores relativos às condições de saúde;
> - Implementar mecanismos de apoio para o aperfeiçoamento permanente da produção de dados e informações;
> - Promover intercâmbio com outros subsistemas especializados de informação da administração pública;
> - contribuir para o estudo de aspectos de reconhecida relevância para a compreensão do quadro sanitário brasileiro; e
>-  Formar mecanismos indutores do uso de informações essenciais para a orientação de processos decisórios no âmbito do SUS.
.
## Desenvolvimento
#### Produtos e desenvolvimento da Ripsa

----------

[2]O primeiro produto finalístico da Ripsa é a publicação regular de **Indicadores e Dados Básicos (IDB)**, que sistematiza informações essenciais para compreensão geral da situação de saúde e acompanhamento de suas tendências. Nos seus primeiros anos de atuação, a Rede dedicou-se ao processo de construção coletiva do IDB e a aperfeiçoar progressivamente esse produto, assegurando a sua disponibilidade regular.
 Diversos CTI têm apoiado a construção do IDB, em temas como:

 * Padronização de atributos comuns dos sistemas de informação
infantil, perinatal e materna.
 * Capacitação do profissional de informações.
 *  Análise de dados espaciais em saúde; sala de situação de saúde.
 * Saúde, seguridade social e trabalho.
 * Alimentação e nutrição.
 * Saúde do idoso.
 * Monitoramento do plano nacional de saúde.
 * Acidentes e violência.
 * Informação de base populacional.
 * Saúde sexual e reprodutiva.
 * Informes de situação e tendências.
 * Iniciativa Ripsa em âmbito estadual.


[1]Ripsa passou a contar com maior participação e suporte do Ministério da Saúde, podendo dedicar maior atenção ao segundo produto substantivo da Ripsa: a elaboração de **Informes de Situação e Tendências (IST)**. Também deu início a um processo de aplicação da metodologia **Ripsa em âmbito estadual**, que visa promover a qualidade e o uso da informação nos sistemas locais de saúde. Desenvolve-se ainda o Portal da [2]**Ripsa na Biblioteca Virtual de Saúde** (BVS-Ripsa), que amplia o acesso a produtos e metodologias de trabalho da Rede, propicia vínculo com as bases de informação científico-técnica, e favorece a cooperação internacional (http://www.ripsa.org.br).


----------
[1] Ministério da Saúde. Portaria Nº 495 GM de 10.03.2006, publicada no DOU de 13.03.2006.
[2] Ministério da Saúde. Organização Pan-Americana da Saúde. Quinto Termo de Ajuste ao Termo de Cooperação nº 14 firmado em 12.06.2006, que adiciona recursos para desenvolvimento da Ripsa até 2010.



### ***METeOR***

----------
 [3]Repositório On-line de Metadados da Austrália para padrões nacionais de metadados para os setores de saúde, cuidados a idosos, serviços comunitários, primeira infância, habitação e desabrigados.
O METeOR é administrado pelo [(Instituto Australiano de Saúde e Bem-Estar)] (AIHW) e foi desenvolvido pela primeira vez em 2005 para substituir o repositório anterior, o Knowledgebase.

### Como funciona o METeOR

[3]Opera como um registro de metadados projetado para suportar uma abordagem disciplinada para o desenvolvimento, armazenamento e gerenciamento de metadados, em conformidade com o padrão internacional de modelagem de informações ISO / IEC 11179 lançado em 2003.

> **O METeOR fornece aos usuários um conjunto de recursos e ferramentas para suportar o desenvolvimento de metadados:**

> -  Acesso on-line a definições de dados endossadas nacionalmente (padrões de metadados)
   > - Ferramentas para criar novas definições baseadas em componentes já endossados ​​existentes um recurso de pesquisa para ajudar a    encontrar metadados rapidamente
   > - Um fórum colaborativo para permitir que os usuários interajam no desenvolvimento de metadados
   > - Acesso a processos de revisão e aprovação dentro do sistema.

## Indicadores e padrões de metadados
#### Os metadados costumam ser chamados de "dados sobre dados".

----------
[3]O desenvolvimento de indicadores no METeOR melhora a qualidade, relevância, consistência e a disponibilidade de informação nacional sobre a saúde e o bem-estar dos australianos. Os impulsionadores para o desenvolvimento padrão surgem da necessidade de melhor informação - seja ela estatística, administrativa, clínica ou outra informação. Isso garante que os dados usados ​​nas estatísticas sejam compatíveis e facilita a interoperabilidade nacional e internacional.

### Os benefícios dos padrões de metadados incluem:

>- **Consistência de conteúdo e definição** , garantindo que os usuários de dados possam entender claramente o significado,    independentemente de como os dados são coletados ou armazenados.    Padrões fornecem transparência de definições de dados.
   >- **Duplicação e diversidade reduzidas** , fornecendo uma plataforma comum e consistente para as organizações trabalharem; simplificar o    processo de desenvolvimento através da reutilização de padrões    existentes; e melhorar o caminho para adoção e implementação nos    níveis local e nacional.

### Dicionários nacionais de dados
[3]Os dicionários nacionais de dados são uma referência de termos e protocolos padronizados e aceitos utilizados para a coleta de dados nos setores de Saúde, Serviços Comunitários e Habitação. Esses termos refletem uma vasta gama de serviços de saúde, serviços comunitários, habitação e pesquisa.



#  Dataverse

### Software de repositório de dados de pesquisa de código aberto
### Pesquisadores
### O que é um dataverse?

[5]Ter um dataverse permite compartilhar, organizar e arquivar seus dados, além de poder exibi-lo em seu site. Ele garante que você receba crédito por seus dados por meio de [citações formais de dados acadêmicos](http://dataverse.org/best-practices/academic-credit) e ajuda a satisfazer os requisitos de compartilhamento de dados de financiadores e editores. Você também tem controle total de seus conjuntos de dados, de quem você compartilha seus dados e quando publicar. Sua visibilidade na web também é aumentada, pois é pesquisável junto com os dados de pesquisa de outros acadêmicos.
### Revistas
[5]A publicação dos dados de pesquisa de seus autores nos repositórios do Dataverse aumenta o impacto do seu diário:

>-   Preservar os dados e torná-los citáveis, seguindo as melhores práticas que melhoram “a robustez e a reprodutibilidade da ciência” ( [Cousijn et al., 2017](https://doi.org/10.1101/097196) ; [Fenner et al., 2016](https://doi.org/10.1101/100784) )
>-   Ajudar os autores a cumprir os mandatos de compartilhamento de dados dos financiadores
>-   Aumente o crédito que os autores recebem pela reutilização de seus dados. ( [Data Citation Synthesis Group, 2014](https://www.force11.org/group/joint-declaration-data-citation-principles-final) )


### Instituições

### Usando o Harvard Dataverse para sua instituição

[5]Sua instituição precisa de um local para hospedar dados de pesquisa, mas não precisa instalar o software Dataverse? Usando o [Harvard Dataverse](https://dataverse.harvard.edu/) , qualquer instituição pode criar um dataverse personalizado para pesquisadores, departamentos e professores compartilharem seus dados de pesquisa.

### Desenvolvedores

[5]A Comunidade de Desenvolvimento do Dataverse é um grupo ativo de colaboradores internos e externos para a base de código do software Dataverse. Desde a correção de erros até a gravação de documentação, bem como a criação de integrações e bibliotecas de clientes, a comunidade é uma parte importante do que torna o software Dataverse bem-sucedido. Envolva-se contribuindo, integrando ou participando.

### Projeto Dataverse
[4]É um aplicativo da Web de código-fonte aberto para compartilhar, preservar, citar, explorar e analisar dados de pesquisa. Ele facilita a disponibilização de dados para outras pessoas e permite que você replique o trabalho de outras pessoas com mais facilidade. Pesquisadores, periódicos, autores de dados, editores, distribuidores de dados e instituições afiliadas recebem crédito acadêmico e visibilidade na web.


### A percepção central por trás do Dataverse

[4]Automatizar grande parte do trabalho do arquivista profissional, fornecer serviços e distribuir crédito ao criador de dados. Antes do Dataverse, os pesquisadores foram forçados a escolher entre receber crédito por seus dados, controlar a distribuição, mas sem garantias de preservação a longo prazo, ou ter garantias de preservação a longo prazo, enviando-a para um arquivo profissional, mas sem receber muito crédito. O Dataverse quebra essa má escolha: colocamos um Dataverse (um arquivo virtual) em seu site que tem a aparência, o branding e a URL do seu site, além de uma citação acadêmica para os dados que oferecem crédito total e visibilidade da Web. No entanto, essa página do seu site é servida por um repositório Dataverse, com apoio institucional e garantias de preservação a longo prazo. Veja Gary King. 2007. "[Uma Introdução à Rede Dataverse como uma Infraestrutura para Compartilhamento de Dados](https://gking.harvard.edu/files/abs/dvn-abs.shtml) . ” _Sociological Methods and Research_ , 36, Pp. 173–199.


### Objetivos estratégicos


[4]Os objetivos estratégicos da Dataverse guiam nosso [roteiro de lançamento](http://dataverse.org/goals-roadmap-and-releases) , nossas colaborações com a comunidade e os [serviços que fornecemos](http://dataverse.org/contact) . Atualmente, nossos objetivos são:

>- aumentar a adoção (usuários, dataverses, conjuntos de dados, instalações, periódicos)

>- Concluir os recursos de migração do Dataverse 4

>- desenvolver capacidade para lidar com dados de fluxo, em grande escala e sensíveis a nível 3

>- expandir recursos de dados e metadados para disciplinas existentes e novas

>- expandir recursos de arquivamento e preservação

>- aumentar as contribuições da comunidade de desenvolvimento de código aberto

>- melhorar o UX e a interface do usuário

>- continuar a aumentar a qualidade do software  


### Colaboração

[4]O Instituto de Ciências Sociais Quantitativas (IQSS) colabora com a Harvard University Library e a Harvard University para tornar a instalação do [Harvard Dataverse](http://dataverse.harvard.edu/) disponível abertamente para pesquisadores e coletores de dados de todas as disciplinas, para o depósito de dados. O IQSS lidera o desenvolvimento do software de código aberto Dataverse e, com o [Open Data Assistance Program em Harvard](http://projects.iq.harvard.edu/odap/home) (uma colaboração com a Harvard Library, o Office for Scholarly Communication e IQSS), fornece suporte ao usuário. Os Serviços de Tecnologia de Bibliotecas da HUIT oferecem suporte a hospedagem e backup do Harvard Dataverse.



## A história


[4]O Dataverse está sendo desenvolvido no [Instituto de Ciências Sociais Quantitativas de](http://www.iq.harvard.edu/product-development) Harvard [(IQSS)](http://www.iq.harvard.edu/product-development) , juntamente com muitos colaboradores e colaboradores em todo o mundo. O Dataverse foi construído com base em nossa experiência com nosso projeto anterior de Virtual Data Center (VDC), que abrangeu o período de 1999-2006 como uma colaboração entre o Data Center Harvard-MIT (agora parte do IQSS) e a Biblioteca da Universidade de Harvard. Os precursores do VDC datam de 1987, compreendendo entidades como software pré-web para transferir automaticamente informações de catalogação por FTP para outros sites em todo o campus automaticamente em horários designados e, antes disso, para um guia de software independente para dados locais.


### Conclusão

Ripsa é uma excelente Estratégia de cooperação para o desenvolvimento de indicadores de saúde para ser implementada pelo DAF, para formar mecanismos indutores do uso de informações essenciais para orientação de processos e poio para tomada de decisões e estabelecer uma base de informações essenciais e consistentes para a análise das condições da saúde do país.
O banco de dados GreenPlum é adequado a necessidade do DAF, para gestão de grandes volumes de dados permitindo consultas e inserções massivas e paralelas com foco em analitcs, inteligência e aprendizado de máquina. Permitir o desenvolvimento de forma escalável da proposta que o DAF procura desenvolver.
Fazendo o uso do Greenplum no DAF permitirá reduzir os pontos de armazenamentos de dados fornecendo um ambiente único e escalável para convergir cargas de trabalho analíticas e operacionais, como o fluxo de processamento. Execução consultas pontuais, ingestão rápida de dados, exploração de dados científicos e consultas de relatórios de longa duração com maior escala e simultaneidade.
O uso do dataverse (aplicativo da Web de código-fonte aberto) no DAF permitirá compartilhar, preservar, citar, explorar e analisar dados de pesquisa organizando e arquivando seus dados, além de poder exibi-lo em site. Ele garante receba crédito por seus dados por meio de [citações formais de dados acadêmicos](http://dataverse.org/best-practices/academic-credit) e ajuda a satisfazer os requisitos de compartilhamento de dados.
O repositório METeOR é excelente ao DAF, pois o desenvolvimento de indicadores de metadados melhora a qualidade, relevância, consistência e a disponibilidade de informação nacional sobre a saúde e o bem-estar do país.
Possui ferramentas para criar novas definições baseadas em componentes já endossados ​​existentes um recurso de pesquisa para ajudar a encontrar metadados rapidamente.



### Referências
[1] NOTÍCIAS RIPSA. DATASUS, MINISTÉRIO DA SAÚDE. Disponível em: [<http://datasus.saude.gov.br/noticias/58-ripsa>](http://datasus.saude.gov.br/noticias/58-ripsa). Acesso em: 05 ago. 2019.
[2] Produtos e desenvolvimento da Ripsa. RIPSA, BIBLIOTECA VIRTUAL EM SAÚDE. Disponível em: <http://www.ripsa.org.br/vhl/metodologia-ripsa/desenvolvimento/>. Acesso em: 05 ago. 2019.
[3] SOBRE METADADOS. METEOR,  INSTITUTO AUSTRALIANO DE SAÚDE E BEM ESTAR. Disponível em: <https://meteor.aihw.gov.au/content/index.phtml/itemId/181414>. Acesso em: 05 ago. 2019.
[4] SOBRE O PROJETO. Dataverse, Harvard.edu. Disponível em: [<https://dataverse.org/about>]. Acesso em: 07 ago. 2019.
[5] OPEN SOURCE RESEARCH DATA REPOSITORY SOFTWARE. Dataverse, Harvard.edu. Disponível em: [<https://dataverse.org/>]. Acesso em: 07 ago. 2019.
