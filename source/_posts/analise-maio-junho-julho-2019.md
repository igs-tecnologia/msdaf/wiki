---
title: Análise repasses Mai, Jun, Jul 2019
toc: true
sidebar:
    left:
        sticky: true
widgets:
-
    type: toc
    position: left
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
- [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
- [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---

Este trabalho se refere à análise dos dados do Farmácia Popular dos meses de Maio, Junho e Julho de 2019. Além da demanda de encontrar outliers e/ou motivos para a diferença de valores, visando o entendimento dos dados.

# Desenvolvimento
Foi desenvolvida uma query SQL para a consulta dos dados no BD utilizando a VW_DISPENSACAO, conforme a baixo:

```sql
-- Dispensacao
SELECT DT_DISPENSACAO, CO_SEQ_DISPENSACAO, QT_DISPENSACAO, NU_COMPETENCIA_DISPENSACAO, VL_UNITARIO, VL_REFERENCIA_POPFARMA,
-- Estabelecimento
CO_SEQ_ESTABELECIMENTO,
-- CID
NO_CID, CO_CID, ST_REGISTRO_ATIVO_CID,
-- Paciente
ST_VIVO, CO_PESSOA, DT_NASCIMENTO, DS_MES_NASCIMENTO, CO_ANO_NASCIMENTO, SG_SEXO_PACIENTE,
-- GeoPaciente
CO_MUNICIPIO_IBGE_PAC, NO_MUNICIPIO_PAC, CO_AGLOMERADO_URBANO_PAC, NO_AGLOMERADO_URBANO_PAC, CO_MACRORREGIONAL_SAUDE_PAC,
NO_MACRORREGIONAL_SAUDE_PAC, CO_MESORREGIAO_PAC, NO_MESORREGIAO_PAC, CO_MICRORREGIAO_PAC, NO_MICRORREGIAO_PAC, CO_MICRORREGIONAL_SAUDE_PAC,
NO_MICRORREGIONAL_SAUDE_PAC, SG_UF_PAC, NO_UF_PAC, NU_ALTITUDE_MUN_PAC, NU_UF_LONGITUDE_PAC, NU_LONGITUDE_MUN_PAC, NO_REGIAO_SAUDE_PAC,
-- Participacao Programas Paciente
ST_PARTICIPA_POPFARMA_PAC, ST_PART_FARMACIA_POPULAR_PAC,
-- Dados do Produto
NO_PRODUTO, CO_PRODUTO, NU_CATMAT, CO_PATOLOGIA, DS_PATOLOGIA, DS_APRESENTACAO, DS_UNID_APRESENTACAO, SG_UNID_APRESENTACAO, DS_UNID_CONCENTRACAO,
SG_UNID_CONCENTRACAO, QT_MAXIMA, QT_USUAL, CO_PRINCIPIO_ATIVO_MEDICAMENTO,
-- Fonte Produto
NO_FABRICANTE, NU_REGISTRO_ANVISA,
-- Financeiro do Produto
CO_GRUPO_FINANCIAMENTO, DS_GRUPO_FINANCIAMENTO, VL_PRECO_SUBSIDIADO, QT_PRESCRITA, QT_SOLICITADA, QT_ESTORNADA,
-- Programa do Produto
NU_LINHA_CUIDADO, DS_PROGRAMA_SAUDE, SG_PROGRAMA_SAUDE, TP_PROGRAMA_SAUDE,
ST_PARTICIPA_POPFARMA_EST, ST_PART_FARMACIA_POPULAR_EST, QT_POPULACA_PORTARIA_1555_2013,
-- Dados adicionais de GeoLoc do Estabelecimento (Farmacia)
CO_AGLOMERADO_URBANO_EST, NO_AGLOMERADO_URBANO_EST, CO_MACRORREGIONAL_SAUDE_EST, NO_MACRORREGIONAL_SAUDE_EST, CO_MESORREGIAO_EST,
NO_MESORREGIAO_EST, CO_MICRORREGIAO_EST, NO_MICRORREGIAO_EST, CO_MICRORREGIONAL_SAUDE_EST, NO_MICRORREGIONAL_SAUDE_EST,
SG_UF_EST, NO_UF_EST, NU_ALTITUDE_MUN_EST, NU_UF_LONGITUDE_EST, NU_LONGITUDE_MUN_EST, NO_REGIAO_SAUDE_EST

FROM DBDMBNAFAR.VW_DISPENSACAO
WHERE "DT_DISPENSACAO" >= DATE '2019-05-01' AND "DT_DISPENSACAO" < DATE '2019-07-31' AND CO_ORIGEM_REGISTRO LIKE 'F';
```

# Limitações
Foram encontradas 2 limitações:  

1. Colunas com erro na consulta:
    ```sql
    --Colunas com Problema (FarmPopQuery):
    --CO_UNIDADE_DISPENSACAO, DS_UNIDADE_SAUDE, NO_MUNICIPIO_ESTAB, SG_UF_ESTAB, NO_REGIAO_ESTAB, CO_MUNICIPIO_IBGE_ESTAB,
    --CO_UNIDADE_CNES, CO_UNIDADE_CNES, NU_LATITUDE_ESTABELECIMENTO, NU_LONGITUDE_ESTABELECIMENTO, NU_CEP_ESTABELECIMENTO,
    --CO_TIPO_UNIDADE, DS_TIPO_UNIDADE
    --DT_OBITO, CO_ANO_OBITO
    ```
2. O DBeaver e/ou devido à limitação de hardware da máquina disponibilizada não foi possível extrair os mais de 58 milhões de registro.

# Soluções para as limitações
1. Colunas com erro dependendo da solução de importação dos dados pelo DAF.

2. Foi desenvolvido um [script em R com o objetivo de extrair estes dados](https://gitlab.com/gaesi/msdaf/cores/extractor/blob/master/oracle_data_source.r).
