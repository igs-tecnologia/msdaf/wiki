---
title: Técnicas de Aprendizado de Máquina.
date: 2019-07-23 10:05:04
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
- [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]

tags:
---

## Lista de Técnicas Aplicáveis no Contexto do MS/DAF
1. __MLP__: 
    1. (Qualifar) __Qualidade dos Dados__ (classificação/identificação de outlier)
    1. (CGMPAF)__Análise preditiva__ multidimensional de demanda por medicamentos tendo o dicionário epidemiológico como dimensões p/ aprendizado e predição
    1. (FarmPop)__Classificação de Fraude__ ao ser treinado sobre uma massa de dados com classes "fraude" e "não fraude" para cada cenário (registros/dimensões)
1. __RNN__ e __LSTM__:
    1. (CGMPAF)__Modelo Generativo (Predição) de uma "futura" Série Histórica__ de demanda por medicamentos (geração de dados futuros)
    1. (CGMPAF)__Regressão Preditiva__ da demanda por medicamentos
    1. (FarmPop)__Classificação Preditiva__ das classes "fraude" e "não fraude" para cada cenário (registros/dimensões)
1. __Autoencoder__:
    1. (Qualifar) __Qualidade dos Dados__ (classificação/identificação de outlier com Autoencoder + Clusterização)
1. __GAN__:
    1. (Qualifar) __Qualidade dos Dados__ (classificação/identificação de outlier)
    1. (FarmPop) __Regressão Preditiva__ da demanda por medicamentos
    1. (CGMPAF) __Classificação Preditiva__ das classes "fraude" e "não fraude" para cada cenário (registros/dimensões)
1. Shallow Learning:
    1. Todos algoritmos podem ser aplicados tanto para Classificação (FarmPop e Qualifar) quanto para Regressão (CGMPAF), mudando apenas o que é citado em cada algoritmo a depender do objetivo 


## Shallow Learning (Redes Neurais de Aprendizado Raso)

### Support Vector Machine (SVM)
O SVM tem um resultado final visualmente similar à regressão linear, porém ao invés de uma reta que melhor separa os dados por quantidade em cada lado, este encontra a melhor reta com uma margem máxima de distanciamento entre os dados de cada categoria. O SVM pode ser usado tanto para regressão quanto para classificação, porém é mais usado para classificação. 
![svm](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/svm.png)
Os dados que geram a margem entre um conjunto e outro são chamados de Support Vectors:
![support_vectors_svm](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/svm_support_vectors.jpg)
Assim como em outros modelos de aprendizado de máquina, o SVM utilizasse do gradiente para ir otimizando sua curvatura e buscando as margens, bem como utiliza-se da função de custo para o ajuste:
![svm_hinge_loss](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/svm_hinge_loss.png)
![svm_loss](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/svm_loss.png)
![svm_gradients](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/svm_gradients.png) 

### Naive Bayes / Random Forest
Naive Bayes é utilizado somente para objetivos dicotômicos (binários/classificação), para os objetivos de dados contínuos (regressão) é utilizado o Random Forest nessa categoria.
Naive Bayes: dado um desfecho e dado uma situação que pode ou não levar ao desfecho é calculada a probabilidade como a fórmula abaixo, sendo o resultado aproximando de 1 para positivo e de 0 para negativo.
![naive-bayes](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/naive-bayes.png)
Random Forest é usado tatno para classificação quanto para regressão a depender da combinação de algoritmos utilizadas, isto porque este algoritmo é uma junção de outros para obter o resultado, podendo utilizar-se de Decision Tree Learning e Bagging.
Algoritmo Bagging:
[bagging_algorithm](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/bagging.svg)
Do Bagging para o Random Forest tendo B como número de exemplos/árvores e podendo ser encontrado usando cross-validation:
[bagging_to_rand](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/bagging_to_rand_forest.svg)

### Decision Tree
Os algoritmos de árvore de decisão usados em aprendizado de máquina são diferentes dos métodos heurísticos, isto porque a arvore de decisão heurística segue regras pré-definidas e em geral este tipo de algoritmo se parece com Sistemas Especialistas. Em sua versão de aprendizado de máquina este algoritmo é chamado de Classification tree (quando para classificar) e Regression trees (quando a saída é um valor contínuo: preço, quantidade...). O processo é feito em 3 etapas:
1. Recursive Binary Splitting: Todas características são consideradas pontos diferentes de split e são testadas usando a função de cálculo de custo. O split com custo mais baixo é selecionado; 
1. Cost of a split:
    1. Regression : sum(y — prediction)²
    1. Classification : G = sum(pk * (1 — pk))
    1. As funções acima são aplicadas para todos os pontos de dados e o custo é calculado para todos splits candidatos  
1. Pruning: remover os ramos (branches) que usem as características com menor importância/impacto

### Regressão Linear
É o modelo que deu origem às teorias e aos algoritmos de aprendizado de máquina. Este algoritmo busca encontrar uma linha que melhor divida os dados para a classificação e também pode ser usado para valores numéricos.
<br>__Principal uso__: Dados linearmente separáveis (como ilustrado na imagem a seguir). 
![LinearRegression](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/linear_regression.gif)

### MultiLayer Perceptron (Rede Neural de aprendizado raso)
A MLP é o início do aprendizado de máquina moderno, assim sendo é bem versátil podendo ser utilizado desde Classificação até Predição. O MLP também é usado como core da maioria das arquiteturas de Deep Learning. Uma Rede Neural Multilayer Perceptron, __caso__ tenha apenas __1 input__ e __1 output__ e __sem função de ativação__ ela será exatamente igual a uma __regressão linear__. No caso do presente trabalho, ao aplicar multiplas dimensões nos clusters e aplicar as funções de ativação e hiperparametros [1]. Na literatura consta que: 
<br>__Principal uso__: Datasets tabulados; Predição Regressiva; Predição Classificativa.
<br>__Tipos de Datasets aplicáveis__: Imagem; Texto; Séries Temporáis.

Representação genérica do Kernel do aprendizado de máquina:
![MachineLearning kernel](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/MachineLearning-Kernel.png)

#### Threshold
Função de ativação que define a saída como 1 ou 0 de acordo com um limite estabelecido.
###### Equação Threshold:
![Threshold Equação](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/threshold_eq.png)
###### Gráfico Threshold:
![Threshold Chart](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/threshold_graph.png)

#### Sigmoid
Comumente utilizada por redes neurais com propagação positiva que precisam ter como saída apenas números positivos, em redes neurais multicamadas e em outras redes com sinais contínuos. __Obs.: em muitos casos em que essa foi empregada, ao fazer benchmark com a Tangente Hiperbólica, a TH obteve melhores resultados__.
###### Equação Sigmoid:
![Sigmoid Equação](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/sigmoid_eq.png)
###### Gráfico Sigmoid
![Sigmoid Chart](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/sigmoid_graph.png)

#### Tangente Hiperbólica
A função de ativação tangente hiperbólica possui uso muito comum em redes neurais cujas saídas devem estar entre -1 e 1.
###### Equação TanH:
![TanH Equação](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/tangent_eq.png)
###### Gráfico TanH:
![TanH Chart](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/tangent_graph.png)

#### Softmax
A função de ativação softmax é usada em redes neurais de classificação. Ela força a saída de uma rede neural a representar a probabilidade dos dados serem de uma das classes definidas. Cada classe é uma "categoria classificatória", ou seja, uma "tag" a ser associada ao input fornecido. Sem ela as saídas dos neurônios são simplesmente valores numéricos onde o maior indica a classe vencedora.
###### Equação Softmax:
![Softmax Equação](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/softmax_eq.png)


## Deep Learning (Redes Neurais de Aprendizado Profundo)
Ao contrário do aprendizado raso que utiliza-se da regressão linear como "core" adicionando inspirações advindas das redes neurais naturais do ser humano, chamadas de funções de ativação e funções de transferência, o aprendizado de máquina profundo utiliza-se do gradiente descendente para encontrar a solução e aprender com os dados.

### Redes Neurais Convolutionárias (ConvNets ou CNN)
As ConvNets são geralmente utilizadas para classificar __imagens__ e raramente, porém bem efetiva, para __áudios__. Basicamente __utiliza-se de uma MLP__ com back propagation em uma rede feedforward com várias camadas ocultas e vários mapas de unidades replicadas em cada camada, sendo capaz de processar e aprender comparando várias classes ao mesmo tempo.
<br>__Principal uso__: classificação de imagens e áudios.
<br>__Tipos de Datasets aplicáveis__: Imagem; Áudio; Texto; Séries Temporáis; Sequência de Entradas.
![ConvNet_Arch](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/deep_learning/convnet.png)

### Redes Neurais Recorrentes (RNN)
O artigo [2] “Finding structure in time” de Jeffrey Elman (1990) descreve a Recurrent Neural Networks (RNNs) __sendo basicamente uma MLP__, porém aponta que a MLP é stateless (sem estado) criando conexões através do tempo, já as RNNs são mais poderosas por combinar 2 propriedades: 1) estados ocultos distribuídos que permitem __guardar muita informação sobre o passado de forma eficiente__; e 2) __dinâmica não linear__ que permite __atualizar os estados ocultos__ de formas complexa, __tornando o aprendizado mais profundo__. Com neurônios e tempo suficiente as RNNs podem computar qualquer coisa que não tenha uma linha do tempo (ex.: áudios e vídeos) e ainda pode executar o treinamento, distribuição do conhecimento e predições em paralelo. <br>
Um grande problema com RNNs é o desaparecimento ou explosão do gradiente descendente que pode ocorrer a depender do tipo de função de ativação usada e/ou da rápida perda de informação ao longo do tempo. Além destes problemas, há o __alto custo computacional__ de __treinamento__, apesar do __baixo custo computacional__ de __predição__. Em geral a principal aplicação de RNNs são em casos de sequências, por exemplo autocomplete de texto.
<br> Problemas envolvendo Sequências:
- One-to-Many: Uma observação como entrada mapeada como sequencia para gerar "multiplos passos" como saída
- Many-to-One: Recebe uma sequência de multiplos passos como entrada mapeada para uma classe ou para uma predição quantitativa
- Many-to-Many: Recebe uma sequência de multiplos passos como entrada mapeada para uma sequência com multiplos passos como saída

<br>__Principal uso__: autocomplete de textos e/ou predição de série histórica
<br>__Tipos de Datasets aplicáveis__: Texto, Fala/Discurso; Classificação Preditiva; Regressão Preditiva; Modelos Generativos (o algoritmo criar algo novo).
<br>__Tipos de Datasets não utilizáveis__: Dados tabulados; Imagens.
![RNNs_Arch](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/deep_learning/rnns.jpeg)

### Long/Short Term Memories (LSTM)
[3] Hochreiter & Schmidhuber (1997) __resolveu o problema__ de recordação/__memória de longo prazo__ das __RNNs__ construindo o que ficou conhecido como Long-Short Term Memory (LSTMs). LSTMs combatem o desaparecimento e/ou explosão do gradiente descendente introduzindo portões/intermediários e uma célula de memória explícita. LSTMs têm se demonstrado capaz de aprender sequências complexas, tal como escrever como Shakespeare ou compor músicas "primitivas" 
<br>__Principal uso/datasets__: os mesmos da RNN, já que se trata de uma RNN melhorada
![LSTM_Arch](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/deep_learning/lstm.jpeg)

### Unidade Recorrente Bloqueada (Gated Recurrent Unit = GRU)
Esta arquitetura é uma __leve melhoria na LSTM__ sem o uso da camada de célula da LSTM para passar valores e também o uso de duas entradas necessárias: X_train e H_previous. O calculo em cada iteração garante que os valores de H_current sendo passados retenha um grande montante de informações antigas ou são iniciados com um salto com o maior número de informação nova. Maiores detalhes sobre GRUs estão disponíveis no artigo “Empirical evaluation of gated recurrent neural networks on sequence modeling” (Junyoung Chung, 2014) [4].     
<br>__Principal uso/datasets__: os mesmos da LSTM e RNN, já que se trata de uma LSTM melhorada

### Redes de Crença Profundas (Deep Belief Networks)
A maioria das redes neurais utilizam back-propagation para calcular o erro da contribuição de cada nó. Porém existem alguns problemas com o back-propagation, entre eles está a necessidade de haver uma "label"/classe para cada dado, sendo que alguns dados não é possível classificar. Outro problema da back-propagation é o custo computacional no caso de muitas camadas ocultas. Por fim, o terceiro problema é ficar presa em um "ótimo local" e não achar ótimos mais globais. Assim sendo, utiliza-se a abordagem de aprendizado não-supervisionado mantendo a simplicidade e a eficiência do uso do gradiante descendente para ajustar os pesos. Yoshua Bengio trouxe o termo Deep Belief Networks em 2007 no artigo “Greedy layer-wise training of deep networks” [5], no qual demonstrou quão eficiente e treinável este algoritmo é em lotes/pilhas.
<br>Deep Belief Networks podem aprender a representar o dado como um modelo probabilístico. Uma vez treinado e convergido como um estado estável através do aprendizado não-supervisionado, o modelo pode ser usado para gerar novos dados. 
![DeepBelief_Arch](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/deep_learning/deepbelief.jpeg)

### Autoencoders
O Autoenconder é considerada uma Rede Neural de aprendizado não supervisioando usada para codificar e decodificar dados de forma eficiente. As técnicas de teste de qualidade de dados são aplicadas em dados armazenados em bancos de dados e/ou em data warehouses para detectar violações semânticas ou de restrições (razoabilidade) por exemplo registros com valores não verifiçaveis em um cenário real. O trabalho desenvolvido por Homayouni, Ghosh & Ray em 2018, demonstrou que o uso do Autoencoder como qualificador de dados é viável ao aplicar este em um conjunto de dados (dataset) de dados de saúde [(Homayouni, Ghosh & Indrakshi, 2018)](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/whitepapers/Using-Autoencoder-to-Generate-Data-Quality-Tests.pdf).  
![Autoencoder Arch](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/deep_learning/autoencoder.png)

### Generative Adversarial Network (GAN)
No artigo “Generative adversarial nets” (2014) [6], Ian Goodfellow apresentou uma nova rede neural que trabalha com 2 redes neurais juntas. Generative Adversarial Networks (GANs) consiste em 2 redes (geralmente combinando uma rede Feed Forwards e uma Convolutional Neural Nets), sendo uma para gerar o conteúro (generativa) e outra que julga o conteúdo (discriminativa). A tarefa do gerador e criar uma imagem que pareça natural e simular a uma original. A tarefa da rede discriminativa é prevê o que é a imagem e receber o feedback da rede geradora. No artigo "Synthesizing Tabular Data using Generative Adversarial Networks", Lei Xu & Kalyan Veeramachaneni [7] demonstraram que é possível utilizar esta mesma técnica para dados tabulares como é o caso do MS/DAF.
![GAN Arch](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-machine-learning/deep_learning/gan.png)

## Referências
[1] Rosenblatt, Frank. “The perceptron: a probabilistic model for information storage and organization in the brain.” Psychological review 65.6 (1958): 386.

[2] Elman, Jeffrey L. “Finding structure in time.” Cognitive science 14.2 (1990): 179–211.

[3] Hochreiter, Sepp, and Jürgen Schmidhuber. “Long short-term memory.” Neural computation 9.8 (1997): 1735–1780.

[4] Chung, Junyoung, et al. “Empirical evaluation of gated recurrent neural networks on sequence modeling.” arXiv preprint arXiv:1412.3555 (2014).

[5] Bengio, Yoshua, et al. “Greedy layer-wise training of deep networks.” Advances in neural information processing systems 19 (2007): 153.

[6] Goodfellow, Ian, et al. “Generative adversarial nets.” Advances in Neural Information Processing Systems. 2014.

[7] L. Xu and K. Veeramachaneni, “Synthesizing Tabular Data using Generative Adversarial Networks,” Nov. 2018.
