---
title: Estudo de técnicas para Análise de Predições
date: 2019-03-12 10:14:15
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

tags:
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
- [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]


---
O objetivo deste documento é formalizar uma série de técnicas e estudos que podem ser utilizados para analisar os dados referentes as predições feitas pelo algoritmo atualmente utilizado no Ministério da Saúde com a finalidade de calcular a quantidade de medicamentos que devem ser dispensados para cada estado segundo as posições de estoque e os registros passados de solicitação de cada estado/município.
Existem grandes obstáculos para se identificar estratégias e para se alcançar resultados eficientes para predição de dispensação devido ao registro histórico de posição de estoque estar fundamentado em dispensações aleatórias ou com baixa acurácia. No entanto, é possível levantar hipóteses e avaliar critérios práticos levando em consideração o volume de dados para se identificar quais são as falhas nas quais incorre o algoritmo atual e quais seriam as possibilidades de melhoria das predições e suas limitações considerando o histórico de dados corrente.
Para tal, esse documento pretende apresentar um conjunto de propostas para análise, classificação e apresentação dos dados de posição em estoque de cada estado ou município para, a partir delas, apresentar critérios e indicadores que apresentem os porquês que respondam a três perguntas:
* Quais são as possibilidades de melhoria das predições de dispensação?

* Quais são os algoritmos que apresentam melhor adequação ao problema?

* Quais seriam os resultados práticos da utilização de tais algoritmos?

O estudo inicial para se identificar os problemas críticos de logística relacionados ao processo de distribuição de medicamentos é apresentado também, para embasar as abstrações, dimensionamentos de atributos, projeções e cálculos matemáticos utilizados.
## **Problemas na distribuição de medicamentos**

O processo de dispensação de medicamentos pelo Ministério da Saúde acontece de duas formas, a primeira e principal é por meio de solicitação trimestral justificada dos estados e municípios que apresentam as quantidades que acreditam ser ideais. Caso ocorra alguma divergência nas quantidades de medicamentos que o estado solicitou e que efetivamente foram consumidos, o estado pode solicitar uma compensação nos meses posteriores para evitar a falta de medicamentos. O grande problema reside em vencer o processo burocrático e logístico interno para receber os medicamentos dentro do prazo que os estoques ainda existam e evitar que os pacientes fiquem grandes períodos a espera do recebimento de sua medicação.
A proposição que guia a distribuição de medicamentos pelo Departamento de Assistência Farmacêutica é a seguinte:
* *"Enviar a melhor quantidade de medicamentos para todos os estados e municípios dentro do prazo ideal para manutenção dos estoques locais."*

Essa afirmação parece simples de ser compreendida, mas engloba dois conceitos críticos que possuem características bastante complexas de se analisar: *melhor quantidade*e *prazo ideal*.
A melhor quantidade de medicamentos é uma medida demasiada complexa pois, caso muitos medicamentos sejam enviados, o custo supera o orçamento previsto e se torna inviável executar outras dispensações futuras, na medida que, caso a quantidade seja baixa demais, o custo com logística de distribuição sobe, devido ao aumento do número de compensações futuras para manutenção dos estoques.
Para reduzir os custos e prover melhor distribuição, é preciso então encontrar um ponto de equilíbrio entre prazo e quantidade. De forma que o custo total seja o menor possível. A figura abaixo ilustra uma curva hipotética que apresenta uma abstração simplista da relação entre o custo com distribuição e aquisição e a quantidade de medicamentos dispensada por vez.

![](../images/post-estudo-alg-preditivo/image1.png)

Nesse cenário hipotético, a soma dos valores das duas curvas simboliza o custo total e os rótulos *mínimo, pouco, muito e máximo,* a quantidade de medicamentos adquirida para distribuição. A grande dificuldade é encontrar o ponto em que as duas curvas se encontram, e quais faixas de valores são representadas por cada um dos rótulos. Utilizando as informações e dados fornecidos, são apresentadas possíveis estratégias para se aproximar a estes resultados.
O cenário real não é generalista como o apresentado e possui outros fatores e características envolvidos que podem ajudar no processo de identificação e classificação de forma que as abstrações façam sentido.
Agora que já conhecemos um pouco mais sobre o problema geral, são apresentados aprofundamentos teóricos no uso dos dados disponíveis e avaliações do conjunto para propor soluções.
OBS: Custo de distribuição > Custo de aquisição
