---
title: Arquitetura de Framework
toc: true
sidebar:
    left:
        sticky: true
widgets:
-
    type: toc
    position: left
categories:
#- [Orçamento, Painel Orçamentário]
#- [Orçamento, PLOA automatizada]
#- [Preditivo, Levantamento Epidemiológico]
- [Preditivo, Tecnologias Aplicadas]
#- [Preditivo, Previsão de compras]
#- [Farmácia Popular, Monitoramento]
#- [Farmácia Popular, Notificação]
#- [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---
# Arquiteturas de Componentes
Baseiam-se em componentes independentes, substituíveis e modulares, elas ajudam a gerenciar a complexidade e encorajam a reutilização.

# Spring Cloud Netflix
Fornece integrações Netflix OSS para aplicativos Spring Boot por meio da autoconfiguração e vinculação ao Spring Environment e a outras expressões do modelo de programação Spring. Com algumas anotações simples, você pode rapidamente ativar e configurar os padrões comuns dentro do seu aplicativo e construir grandes sistemas distribuídos com componentes Netflix testados em batalha. Os padrões fornecidos incluem Descoberta de Serviço **(Eureka)**, Disjuntor ** (Hystrix)**, Roteamento Inteligente **(Zuul)** e Balanceamento de Carga do Lado do Cliente (Fita).


## Spring Cloud:
Responsável por integrar todas as soluções em aplicações Spring Boot, onde toda configuração é feita através de anotações e propriedades do application properties.

### Netflix Eureka
Solução de Service Discovery open source desenvolvida pela Netflix e é composta pelos módulos Eureka Server e Eureka Client.
Eureka Server consiste em uma aplicação que atua como Service Registry permitindo que outras aplicações registrem suas instâncias, com isso,  ele controla os endereços registrados mantendo-os atualizados e sinalizando quando um serviço não está disponível.

### Netflix Hystrix
Biblioteca de tolerância a falhas e latência projetada para isolar pontos de acesso em sistemas remotos, serviços e bibliotecas de terceiros, parar falhas em cascata e habilitar a resiliência em sistemas distribuídos complexos onde a falha é inevitável.

### Netflix Zuul
Porta da frente de todas as solicitações de dispositivos e sites para o back-end do aplicativo de streaming da Netflix. Como um aplicativo de serviço de borda, o Zuul é construído para permitir roteamento dinâmico, monitoramento, resiliência e segurança. Ele também tem a capacidade de encaminhar solicitações para vários grupos do Amazon Auto Scaling, conforme apropriado.

### Netflix Config
Fornece suporte ao servidor e ao cliente para configuração externalizada em um sistema distribuído. Com o Config Server, você tem um local central para gerenciar propriedades externas de aplicativos em todos os ambientes. Os conceitos no cliente e no servidor mapeiam de forma idêntica ao Spring


### Netflix Ribbon
Um balanceador de carga do lado do cliente que lhe dá muito controle sobre o comportamento de clientes HTTP e TCP.


# Service Discovery
Detecção automática de dispositivos e serviços oferecidos por esses dispositivos em uma rede de computadores. Um protocolo de descoberta de serviço ( SDP ) é um protocolo de rede que ajuda a realizar a descoberta de serviços. A descoberta de serviço visa reduzir os esforços de configuração dos usuários.

## O Circuit Breaker
 Mecanismo utilizado pela B3 que permite, na ocorrência de movimentos bruscos de mercado, o amortecimento e o rebalanceamento brasileiro das ordens de compra e de venda. Esse instrumento constitui-se em um "escudo" à volatilidade excessiva em momentos atípicos de mercado.

# Kibana: Incrível ferramenta de análise
Kibana é uma plataforma de analise e visualização de dados, projetada para trabalhar com Elasticsearch. Com ele é possível a compreensão de grandes volumes de dados, sendo possível compartilhar dashboards dinâmicos que exibem alterações em tempo real.

# Elasticsearch
É um servidor de buscas distribuído baseado no Apache Lucene. Foi desenvolvido por Shay Banon e disponibilizado sobre os termos Apache License. ElasticSearch foi desenvolvido em Java e possui código aberto liberado sob os termos da Licença Apache.

# Logstash
Basicamente é uma ferramenta de extração de dados, seu uso é simples, você determina um entrada (input), extrai informações que atendem a um padrão com filtros (filters) e gera uma saída (output), o famoso “entrada -> processo -> saída”.
