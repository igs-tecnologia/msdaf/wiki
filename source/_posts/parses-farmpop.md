---
title: Desenvolvimento dos Parsers de planilhas para dados estruturados
date: 2019-03-11 12:53:41

toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

tags:
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
- [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---

Para a importação de planilhas de dados Qualifar, componentes básicos e dados preditivos no processo de normalização e padronização das informações, foi desenvolvido um *parser (funcionalidade de conversão de arquivos e/ou os dados lá contidos)* para carregamento das planilhas disponibilizadas pelo Ministério da Saúde contendo informações sensíveis para a aplicação de aprendizado de máquina. A arquitetura utilizada nos carregadores de dados foi desenvolvida de forma ampla para permitir a adição de novos cabeçalhos de dados de forma simples, não necessitando o desenvolvimento de um módulo totalmente novo para tal.

Na análise de processos de trabalho do Programa Farmácia Popular também houve a necessidade de desenvolvimento de parser de arquivos, o qual será utilizado para agilidade no processo de multas aplicadas pelo DAF. Foi apresenta a primeira versão do serviço desenvolvido para que fosse aprovado e assim implementado como micro serviço. Abaixo segue a tela da funcionalidade na primeira versão desenvolvida.

![](../images/post-parses-farmpop/image1.png)

Após a apresentação e aprovação estrutural da funcionalidade, a mesma foi implementada como micro serviço conectando-se com a arquitetura já desenvolvida conforme imagem abaixo.
![](../images/post-parses-farmpop/image2.png)
Para utilização da funcionalidade basta seguir o manual contendo o modo de exportação do arquivo CSV no site do SNA (Sistema Nacional de Auditoria do SUS) , com a exportação do arquivo no formato indicado no manual o usuário deverá acessar a opção de IMPORTAR CSV e no campo disponibilizado incluir o arquivo CSV já baixado, com isso após a conversão automaticamente o serviço disponibilizará o arquivo TXT já convertido no formato exigido pelo site do TCU.

## **Solicitação de Esclarecimento**
Após o levantamento dos processos da Farmácia Popular, no que tange a notificação, foi percebida a demanda por desenvolver um mecanismo que permitisse um ganho de produtividade na produção das Solicitações de Esclarecimentos de pendências documentais sobre os indícios encontrado na etapa anterior do processo. Assim sendo, foi desenvolvido um item "Pedido de Esclarecimento" na sessão Notificação.

![](../images/post-parses-farmpop/image3.png)
## **Manter Tipos de Ocorrência**
Com o objetivo de aumentar a produtividade, foi desenvolvida uma ferramenta de manter Tipos de Ocorrências (cadastrar, excluir, alterar e pesquisar), para aumentar a produtividade ao associar Tipo de Ocorrência à transação.
![](../images/post-parses-farmpop/image4.png)

## **Manter Pedido de Esclarecimento**
Ainda com o objetivo de aumentar a produtividade, foi desenvolvida uma ferramenta de manter Pedidos de Esclarecimento (cadastrar, excluir, alterar e pesquisar), tendo como base o Número Único do Processo - (NUP/SEI) e associando as transações com suas devidas ocorrências para por fim gerar o texto do corpo da Solicitação de Esclarecimento oficial no SEI.
![](../images/post-parses-farmpop/image5.png)
