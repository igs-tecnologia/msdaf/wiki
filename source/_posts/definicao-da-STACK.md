---
title: Estudo e definição de Stack para ambiente de desenvolvimento e produção.
date: 2019-03-11 12:26:10
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

tags:
categories:
#- [Orçamento, Painel Orçamentário]
#- [Orçamento, PLOA automatizada]
#- [Preditivo, Levantamento Epidemiológico]
#- [Preditivo, Previsão de compras]
 - [Preditivo, Tecnologias Aplicadas]
#- [Farmácia Popular, Monitoramento]
#- [Farmácia Popular, Notificação]
#- [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---

### Situação inicial no DAF
Na análise inicial do ambiente tecnológico já utilizado pelo DAF foi identificado que os profissionais utilizam planilhas o qual preenchem com os dados necessários para os trabalhos executados, também utilizam sistemas como Horus, BNAFAR, etc. Esses dados são armazenados em estruturas de banco de dados. Com esta análise executada foi identificado os tipos de extrações necessárias para a desenvolvimento e execução dos algoritmos preditivos.
Foi identificado também que não havia uma ambiente de aplicação para o desenvolvimento de ferramentas necessárias para o trabalho. Esse ambiente será necessário para receber as aplicações propostas pelas pesquisas que estão sendo realizadas.

### Descobertas com estudos no DAF
Foram realizados os estudos de tecnologias, metodologias, plataformas e ferramentas para o desenvolvimento dos estudos e pesquisas necessários para implementação do projeto.

**Arquitetura dos ambiente da aplicação**
Para o desenvolvimento das ferramentas, aplicações, algoritmos entre outras entregas propostas pelo projeto, foi identificado a necessidade de uma arquitetura de desenvolvimento composta por ferramentas, frameworks, metodologias entre outras. Com isso, foram identificadas as tecnologias necessárias à integração com a arquitetura proposta pelo DATASUS. Com as análises feitas, adotou-se a arquitetura de micro-serviço e suas ferramentas.

**STACK de ferramentas DATASUS**
Foi estudado todas a arquitetura de ferramentas utilizada em desenvolvimento, baseado nas diretrizes do DATASUS. Com o mesmo foi possível identificar o modo de utilização de cada ferramenta a fim de equacionar os desenvolvimentos do projeto com as diretrizes aplicadas.
![](../images/post-definicao-da-stack/MicroService-Diagram-RedHat-DevOps.png)


**Estudo do ecossistema Docker**
Para a construção das imagens e disseminação de conhecimento, foi produzido também um documento sobre os estudos realizados utilizando o esquema de containers, mais especificamente o Docker, para ambiente de desenvolvimento, explicitando suas características, vantagens, desvantagens e formas de aplicação. Bem como uma apresentação mais aprofundada sobre a estrutura interna de comunicação entre serviços e nós cadastrados em uma rede de containers Docker.

**Definição da Arquitetura e do STACK de ferramentas/frameworks**
Para que o desenvolvimento dos estudos e códigos possam acontecer e permitindo a sua disponibilização em diferentes contextos, foram preparados os ambientes com foco em reprodutibilidade e em replicabilidade, ou seja, a mesma funcionalidade pode ser utilizada por várias outras, consumindo os dados ou ações geradas sem precisar ter acesso direto ao código. o Desenvolvimento será por o uso de sistemas de containers Docker e a disponibilização dos estudos em notebooks python. Para tal, foram definidas as estruturas básicas para replicação dos ambientes em desenvolvimento, homologação e produção, como já dito, em conformidade com a stack utilizada no ministério. Com isso foi desenvolvida toda a estruturação e ferramentas bases necessária para a aplicação destes conceitos onde serão acopladas as ferramentas e funcionalidade desenvolvidas no projeto.

Foi realizada uma pesquisa para que selecionar as melhores tecnologias e ferramentas para aplicação no projeto. Com a visão total do projeto e com a pesquisa chegou-se a conclusão que para que se tenha aumento de disponibilidade, tempo de resposta recorde e baixa latência os micro serviços seriam a solução ideal. A estrutura de micro serviços que é uma técnica de desenvolvimento de software baseada em em serviços pequenos ligeiramente acoplados e que permitem a entrega e a implantação contínua de serviços de softwares que podem se tornar grande, complexos e que necessitam de uma alta escalabilidade. As imagens abaixo demonstram em diagrama como funcionará a arquitetura resultante da pesquisa realizada:
![](../images/post-definicao-da-stack/MicroService-Diagram1.jpg)

![](../images/post-definicao-da-stack/MicroService-Diagram-2.jpg)

![](../images/post-definicao-da-stack/componentes.jpg)

A seguir será demonstrado as ferramentas que serão utilizadas bem como o papel que cada uma irá exercer na arquitetura.
**Docker**
O conjunto de ferramentas que compõem o ecossistema de micro serviços em desenvolvimento, provê uma série de recursos necessários para que as aplicações vindouras de inteligência e automatização sejam eficientes, tanto para os operadores do sistema quanto para a equipe técnica do ministério. Nesse sentido, o esforço inicial em se construir esse ferramental incluiu a construção de scripts para automatização do processo de configuração do ambiente, utilizando o Docker como meio de execução do ecossistema, já em adequação a estrutura de deploy atualmente empregada no ministério, juntamente com OpenShift e Kubernetes.

O funcionamento do docker será da seguinte forma: A aplicação padrão do docker controla o estado inicial de uma máquina que dirá quais aplicações e serviços esse ambiente possui. Em uma máquina convencional, caso esse estado não esteja exatamente como esperado, podem surgir falhas e dificuldades no processo de automatização de operações. Com o uso de containers, o ambiente simulado permite recriar exatamente uma máquina com o estado inicial que se espera, esse estado é chamado de imagem. O conjunto de instruções interpretados pelo Docker para construção da imagem é lido do arquivo Dockerfile que pode ser versionado e controlado para estar sempre no estado ideal para configuração do serviço disponibilizado.
O diagrama a seguir ilustra o funcionamento e o conjunto de aplicações que provêm interfaces entre Docker e Kernel.
![](../images/post-definicao-da-stack/Docker-linux-interfaces.png)Docker-linux-interfaces.png
Foram geradas imagens para as ferramentas Eureka, Zuul e ConfigServer que, juntamente com o Kibana, Elastic e Logstash, são a base para o desenvolvimento dos novos serviços. As imagens geradas para o projeto foram disponibilizadas na plataforma DockerHub, que mantém imagens públicas para reuso, como ilustra a figura abaixo.



![](../images/post-definicao-da-stack/dockerhub.png)



**Openshift**	A arquitetura e stack de aplicações proposta utilizando o Docker como mecanismo de execução foi testado em um estudo de homologação preliminar em um ambiente OpenShift utilizando o Kubernetes como provisionador do ambiente. Para os testes, foi configurado um servidor CentOS 7 (1810), no qual foram instaladas as seguintes aplicações e suas respectivas versões: OpenShift versão 1.4.1 (+3f9807a), Kubernetes versão 1.4.0 (+776c994), etcd versão 3.1.0 (-rc.0).

Para execução do ambiente foram utilizadas as configurações padrões de inicialização de rotas e clusters do OpenShift sem a necessidade de nenhuma configuração adicional. Para a execução dos testes, foram executadas 6 aplicações utilizando como base as imagens publicadas no Dockerhub para inicialização do ambiente. A figura a seguir mostra o ecossistema proposto em funcionamento em um ambiente OpenShift e Kubernetes semelhante ao disponibilizado pelo Ministério da Saúde.

![](../images/post-definicao-da-stack/openshift.png)
**Eureka**	A arquitetura para execução de micro serviços inclui uma série de aplicações para possibilitar a configuração, identificação, busca e comunicação entre serviços disponíveis em uma mesma rede. Para possibilitar a identificação e busca de serviços, o Eureka surge como uma boa opção para registro e manutenção de serviços ativos. Por ser uma aplicação que monitora a disponibilidade dos serviços e registra todos aqueles disponíveis para utilização, o Eureka funciona como a estrutura base para que o ecossistema possa existir. Todas as outras aplicações são inicializadas por meio do registro no Eureka para que possam ser encontradas por outros serviços e também pelo usuário final.

**Zuul**	Apesar de os serviços serem registrados em um mecanismo de descoberta (Eureka), o acesso a cada um ainda é feito de forma descentralizada, exigindo que cada requisição seja direcionada para o domínio e porta específicos de cada serviço, uma aplicação que consome vários serviços para correta execução de suas atividades, precisaria de um grande conjunto de rotas e domínios registrados que, caso um serviço mude, precisaria ser novamente configurado, o que acarreta em um alto risco de indisponibilidade do serviço por erros de resolução de nomes para cada serviço. O Zuul surge como uma alternativa para mitigar essa falha, permitindo que todos os serviços registrados no Eureka possuam uma rota padronizada, dentro de um mesmo domínio provido pelo Zuul. Além da vantagem de centralização dos serviços, o Zuul ainda possibilita o balanceamento de carga, direcionando requisições para serviços replicados de forma equivalente.



**ConfigServer**
O mecanismo de configuração de serviços disponibilizados em uma arquitetura de microsserviços é bastante crítico. Pelo fato de existir uma grande variedade e até mesmo duplicidade de serviços, prover arquivos estáticos de variáveis de ambiente se torna ineficiente e pode acarretar na indisponibilidade de serviços caso as configurações exijam a reinicialização de um conjunto de serviços. Para solucionar este problema, é utilizado o servidor de configurações (Spring Cloud Config Server) que armazena todas as variáveis de ambiente para cada serviço possibilitando a definição por diferentes ambientes. Dessa forma, sempre que alterações ocorrem na definição das configurações, os próprios serviços podem identificá-las e se adaptarem sem a necessidade de reinicialização. Além disso, a gestão centralizada de configurações facilita o controle e organização dos múltiplos ambientes disponibilizados para teste, homologação e produção de um ecossistema de software.

![](../images/post-definicao-da-stack/printSpringEureka.png)

Aplicação Eureka, ConfigServer, Zuul(Gateway) e outras em funcionamento
